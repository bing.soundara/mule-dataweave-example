package com.perficient.apimc.mule.ibmmq;

import java.util.LinkedHashMap;

import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.TextMessage;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;

public class MqQueueReader {
	
	private static final String HOST = "localhost";
	private static final int PORT = 1414;
	private static final String CHANNEL = "DEV.APP.SVRCONN";
	private static final String QMGR = "QM1";
	private static final String APP_USER = "app";
	//private static final String APP_PASSWORD = ""
	private static final String QUEUE_NAME = "DEV.QUEUE.1";
	
	private JMSContext context;
	
	public MqQueueReader() {}
	
	public MqMessage readMessages() throws Exception {
		
		JMSProducer producer = null;
		JMSConsumer consumer = null;
		
		JmsConnectionFactory cf = setupConnection();
		JMSContext context = cf.createContext();
		
		Destination destination = context.createQueue("queue:///" + QUEUE_NAME);
		
		
		// sending message
		/*
		long uniqueNumber = System.currentTimeMillis() % 1000;
		TextMessage message = context.createTextMessage("Your lucky number today is " + uniqueNumber);

		producer = context.createProducer();
		producer.send(destination, message);
		System.out.println("Sent message:\n" + message);
		 */
		
		consumer = context.createConsumer(destination); // autoclosable
		Message message = consumer.receive();
		
		return new MqMessage(message);
	}
	
	private JmsConnectionFactory setupConnection() throws Exception {
		
		// Create a connection factory
		JmsFactoryFactory ff;
		try {
			ff = JmsFactoryFactory.getInstance(WMQConstants.WMQ_PROVIDER);
			JmsConnectionFactory cf = ff.createConnectionFactory();
	
			// Set the properties
			cf.setStringProperty(WMQConstants.WMQ_HOST_NAME, HOST);
			cf.setIntProperty(WMQConstants.WMQ_PORT, PORT);
			cf.setStringProperty(WMQConstants.WMQ_CHANNEL, CHANNEL);
			cf.setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
			cf.setStringProperty(WMQConstants.WMQ_QUEUE_MANAGER, QMGR);
			cf.setStringProperty(WMQConstants.WMQ_APPLICATIONNAME, "MuleSoft Poc IbmMq");
			//cf.setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, true);
			cf.setStringProperty(WMQConstants.USERID, APP_USER);
			//cf.setStringProperty(WMQConstants.PASSWORD, APP_PASSWORD);
			
			return cf;
			
		}  catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
			//throw new Exception("Failed to setup JMS Factory");
		}

	}

}
